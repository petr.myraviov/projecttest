<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2',
            'image' => 'required|mimes:jpeg,bmp,png',
            'description' => 'required|min:6',
            'user_id' => 'required|exists:App\User,id',
            'category_id' => 'required|exists:App\Category,id'
        ];
    }
}
